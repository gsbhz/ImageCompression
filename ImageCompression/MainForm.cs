﻿using System;
using System.Windows.Forms;

namespace ImageCompression
{
    /// <summary>
    /// 图片压缩小工具
    /// Bao
    /// 联系：gsbhz@qq.com
    /// </summary>
    public partial class MainForm : Form
    {
        #region MainForm
        public MainForm()
        {
            InitializeComponent();

            init();
        }
        #endregion

        #region 初始化
        private void init()
        {
            this.cmbRate.SelectedItem = "80";

            this.gvList.AutoGenerateColumns = false;
        }
        #endregion

        #region 事件

        #region 选择文件夹
        private void btnSelectSavePath_Click(object sender, EventArgs e)
        {
            this.txtSavePath.Text = Common.SelectFolder();
        }
        #endregion

        // 工具栏事件
        #region 选择单个图片
        private void tbtnAddImg_Click(object sender, EventArgs e)
        {
            //this.gvList.DataSource = Common.Array2List(Common.SelectFiles());
            this.gvList.Append(this.lblProgress, Common.Array2List(Common.SelectFiles()));
        }
        #endregion

        #region 选择文件夹
        private void tbtnAddFolder_Click(object sender, EventArgs e)
        {
            string path = Common.SelectFolder();
            //this.gvList.DataSource = Common.GetAllImg(path);
            this.gvList.Append(this.lblProgress, Common.GetAllImg(path));
        }
        #endregion

        #region 清除选中的文件
        private void tbtnClearItem_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow r in this.gvList.SelectedRows)
            {
                this.gvList.Rows.Remove(r);
            }
            this.lblProgress.Text = "0/" + this.gvList.Rows.Count;
        }
        #endregion

        #region 清空所有文件
        private void tbtnClearAll_Click(object sender, EventArgs e)
        {
            this.gvList.Rows.Clear();
            this.lblProgress.Text = "0/" + this.gvList.Rows.Count;
        }
        #endregion

        #region 开始压缩
        private void tbtnRun_Click(object sender, EventArgs e)
        {
            compression();
        }

        private void btnRun_Click(object sender, EventArgs e)
        {
            compression();
        }
        #endregion

        #region 关于
        private void tbtnAbout_Click(object sender, EventArgs e)
        {

        }
        #endregion

        #endregion

        // =================公共方法=================
        #region 公共方法

        // 压缩
        private void compression()
        {
            ImgCompression c = new ImgCompression(this.gvList.ToList(), this.cmbRate.Text.ToInt(), this.txtSavePath.Text);
            c.ProgressBar = this.progressBar1;
            c.ProgressText = this.lblProgress;
            c.Dgv = this.gvList;
            c.Start();
        }

        #endregion
    }
}
