﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Drawing;
using System.Windows.Forms;

namespace ImageCompression
{
    /// <summary>
    /// 公共方法
    /// Bao
    /// 联系：gsbhz@qq.com
    /// </summary>
    public class Common
    {
        /****************文件操作相关方法****************/
        #region 弹出选择文件对话框
        public static string[] SelectFiles()
        {
            OpenFileDialog f = new OpenFileDialog();
            f.Multiselect = true;
            f.Filter = "jpg图片(*.jpg)|*.jpg|png图片(*.png)|*.png";
            if (f.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                return f.FileNames;
            return default(string[]);
        }
        #endregion

        #region 弹出选择文件夹对话框
        /// <summary>
        /// 弹出选择文件夹对话框
        /// </summary>
        /// <returns>文件夹路径</returns>
        public static string SelectFolder()
        {
            FolderBrowserDialog f = new FolderBrowserDialog();
            if (f.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                return f.SelectedPath;
            return "";
        }
        #endregion

        #region 根据文件夹路径，读取路径下的所有图片文件
        /// <summary>
        /// 根据文件夹路径，读取路径下的所有图片文件
        /// </summary>
        /// <param name="folderPath">文件夹路径</param>
        /// <returns></returns>
        public static List<ImgInfoEntity> GetAllImg(string folderPath)
        {
            if (!Directory.Exists(folderPath)) return new List<ImgInfoEntity>();
            string[] files = Directory.GetFiles(folderPath, "*.jpg", SearchOption.AllDirectories);
            return Array2List(files);
        }
        #endregion

        #region 把文件列表数组转换成List<ImgInfoEntity>
        /// <summary>
        /// 把数组转换成List
        /// </summary>
        /// <param name="files">文件列表数组</param>
        /// <returns>List</returns>
        public static List<ImgInfoEntity> Array2List(string[] files)
        {
            List<ImgInfoEntity> list = new List<ImgInfoEntity>();

            if (files == null || files.Length <= 0) return list;
            foreach (string s in files)
                list.Add(GetImgInfo(s));

            return list;
        }
        #endregion

        #region 根据路径，读取图片信息
        public static ImgInfoEntity GetImgInfo(string path)
        {
            ImgInfoEntity m = new ImgInfoEntity();
            Image img; //Image.FromFile(path);
            using (FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read))
            {
                img = Image.FromStream(fs);

                m.FileName = Path.GetFileName(path);
                m.Bytes = ByteConversion(fs.Length);
                m.Size = img.Width.ToString() + "×" + img.Height.ToString();
                m.FullPath = path;
            }
            if (img != null) img.Dispose();
            return m;
        }
        #endregion

        // 其它方法
        #region 字节转换成GB、MB和KB
        /// <summary>
        /// 字节转换成GB、MB和KB
        /// </summary>
        /// <param name="KSize"></param>
        /// <returns></returns>
        public static string ByteConversion(long KSize)
        {
            int GB = 1024 * 1024 * 1024;//定义GB的计算常量
            int MB = 1024 * 1024;//定义MB的计算常量
            int KB = 1024;//定义KB的计算常量

            if (KSize / GB >= 1)//如果当前Byte的值大于等于1GB
                return (Math.Round(KSize / (float)GB, 2)).ToString() + " GB";//将其转换成GB
            else if (KSize / MB >= 1)//如果当前Byte的值大于等于1MB
                return (Math.Round(KSize / (float)MB, 2)).ToString() + " MB";//将其转换成MB
            else if (KSize / KB >= 1)//如果当前Byte的值大于等于1KB
                return (Math.Round(KSize / (float)KB, 2)).ToString() + " KB";//将其转换成KGB
            else
                return KSize.ToString() + " 节";//显示Byte值
        }
        #endregion
    }
}
