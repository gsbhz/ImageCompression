﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ImageCompression
{
    /// <summary>
    /// 图片信息
    /// Bao
    /// 联系：gsbhz@qq.com
    /// </summary>
    public class ImgInfoEntity
    {
        /// <summary>
        /// 文件名
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// 文件字节
        /// </summary>
        public string Bytes { get; set; }

        /// <summary>
        /// 新文件字节
        /// </summary>
        public string NewBytes { get; set; }

        /// <summary>
        /// 尺寸
        /// </summary>
        public string Size { get; set; }

        /// <summary>
        /// 新文件尺寸
        /// </summary>
        public string NewSize { get; set; }

        /// <summary>
        /// 文件完整路径
        /// </summary>
        public string FullPath { get; set; }
    }
}
